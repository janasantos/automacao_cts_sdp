*** Settings ***
Library    SeleniumLibrary


*** Variables ***
${url}          https://auth.v2qa.opengalaxy.io/auth/realms/OpenGalaxy/protocol/openid-connect/auth?client_id=opengalaxy-frontend&redirect_uri=https%3A%2F%2Fdemo.semantix.cloud%2F&state=c163dde2-5b3f-4c24-8eec-32b51e4187f0&response_mode=fragment&response_type=code&scope=openid&nonce=c4f7a28d-84c5-4af1-a374-2c85aa073173
${semantix}     //header[@class='bg-grey-6 hide-text bg-image']
${username}     //input[@name='username']
${password}     //input[@id='password']
${login}        //input[@name='login']
${welcome}      //h1[contains(.,'Welcome to SDP, Teste Conta')]
${add-ons}      //span[@class='nav-item__title'][contains(.,'Add-ons')]
${tela}         //strong[contains(.,'Add-ons')]
${wel_pipe}     //strong[contains(.,'Pipelines')]
${pipeline}     //span[contains(.,'Pipeline')]
${job}          //span[contains(.,'Jobs')]
${wel_job}      //h1[@class='sdp-page-title'][contains(.,'Welcome to Jobs')]

*** Keywords ***

Abrir o navegador    
    Open Browser    browser=firefox
    Maximize Browser Window    

Fechar o navegador
    Capture Page Screenshot
    Close Browser

Acessar o SDP 
    Go to    url=${url} 
    Wait Until Element Is Visible    locator=${semantix}

Fazer o login
    Get WebElement                    locator=${username}
    Input Text                        locator=${username}    text=gitat54028@upshopt.com
    Get WebElement                    locator=${password}    
    Input Password                    locator=${password}     password=Teste@123
    Click Button                      locator=${login} 
    Wait Until Element Is Visible     locator=${welcome}
    
Clicar em Add-ons 
    Click Element                     locator=${add-ons}

Validar a tela add-ons
    Wait Until Element Is Visible    locator=${tela}

Clicar em pipeline
    Click Element    locator=${pipeline}

Validar as pipelines criadas no SDP
    Wait Until Element Is Visible    locator=${wel_pipe}  

Clicar em job
    Click Element                    locator=${job}

Validar os jobs criados no SDP
    Wait Until Element Is Visible    locator=${wel_job}


    
    
    
    

    


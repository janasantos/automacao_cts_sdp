*** Settings ***
Documentation    Essa suite testa o portal SDP
Resource         sdp_resources.robot 
Test Setup       Abrir o navegador
Test Teardown    Fechar o navegador


*** Test Cases ***

Caso de teste 1 - Acessar o Add-ons Analytics Chat 
    [Documentation]    Esse teste acessa a página Add-ons do SDP
    [Tags]             add-ons
    Acessar o SDP
    Fazer o login
    Clicar em Add-ons
    Validar a tela add-ons

Caso de teste 2 - Validar as pipelines criadas no SDP
    [Documentation]    Esse valida as pipelines no SDP
    [Tags]             valida_pipeline
    Acessar o SDP
    Fazer o login
    Clicar em pipeline
    Validar as pipelines criadas no SDP


Caso de teste 3 - Validar os JOBs criados no SDP
    [Documentation]    Esse teste valida os JOBs no SDP
    [Tags]             valida_job
    Acessar o SDP
    Fazer o login
    Clicar em job
    Validar os jobs criados no SDP

